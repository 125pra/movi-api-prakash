const movieData = require('../movieData.json');
const connection = require("../middlewares/utils/connection").con

async function callToInsert() {
    // let insertAllDirector = await insertDirector();
    // console.log('Add directors inserted');

    //  await insertMovie();
}

function getDirectorData() {
    return new Promise((resolve, reject) => {
        var sql = 'select * from director';
        connection.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result)
            }
        })
    })
}


 function insertDirector() {
        let nameOfDirectors = movieData.map(singleData => singleData["Director"]);
        nameOfDirectors = Array.from(new Set(nameOfDirectors));
     let directorName=   nameOfDirectors.map(DirectorName => {
        return  new Promise((resolve, reject) => {
            var sql = `INSERT INTO director (name) VALUES ("${DirectorName}")`
            connection.query(sql, (err, result) => {
                console.log(`adding ${DirectorName}`)
                if (err) {
                    reject(err);
                } else {
                    resolve(result)
                }
            })
        })

    })
    return Promise.all(directorName);
}

async function insertMovie() {
    var directorDetails;
    await getDirectorData().then(result => {
        directorDetails = result;
    })
    var directorWithId = {}
    directorDetails.forEach(singleData => {
        directorWithId[singleData["name"]] = singleData["directorId"];
    })
    await movieData.forEach(singleData => {
        let rank = singleData["Rank"];
        let title = singleData["Title"];
        let description = singleData["Description"];
        let runtime = singleData["Runtime"];
        let genre = singleData["Genre"];
        let rating = singleData["Rating"];
        let metascore = singleData["Metascore"];
        let votes = singleData["Votes"];
        let grossEarningInMil = singleData["Gross_Earning_in_Mil"];
        let actor = singleData["Actor"];
        let year = singleData["Year"];
        let directorid = directorWithId[singleData["Director"]]
        let sql = `INSERT INTO movie (rank,title,description,runtime,genre,rating,metascore,votes,grossEarningInMil,actor,year,directorid)
             VALUES ("${rank}","${title}" ,"${description}","${runtime}","${genre}","${rating}","${metascore}","${votes}","${grossEarningInMil}","${actor}", "${year}","${directorid}");`

        connection.query(sql, function (err) {
            if (err) {
                console.log(err.message)
            }
        })
    })
}

// console.log(movieData)
// callToInsert();