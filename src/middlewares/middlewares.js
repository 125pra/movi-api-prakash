

const errorHandler=(err, req, res, next)=>{
  res.status(500).send(err.sqlMessage);
  next(err);
};

// const logErrors=(err, req, res, next)=>{
//   console.error(error.stack)
//   next(error);
// };

  module.exports={errorHandler};