const joi = require("joi");

const validateId = joi.object().keys({
    id: joi.number().min(1).required(),
});

const validateDirector = joi.object().keys({
    name: joi.string().min(3).max(30).required(),
});

const validateMovie = joi.object().keys({
    rank: joi.number().min(0).max(100).required(),
    title: joi.string().min(3).max(30).required(),
    description: joi.string().min(3).max(1000).required(),
    runtime: joi.number().min(0).max(10000).required(),
    genre: joi.string().min(3).max(30).required(),
    rating: joi.number().min(0).max(100).required(),
    metascore: joi.number().min(0).max(100000).required(),
    votes: joi.number().min(0).max(10000000).required(),
    grossEarningInMil: joi.number().min(0).max(10000).required(),
    director: joi.string().min(0).max(100).required(),
    actor: joi.string().min(3).max(40).required(),
    year: joi.number().min(1000).max(3000).required(),
});

module.exports = {
    joi,
    validateId,
    validateDirector,
    validateMovie,
};