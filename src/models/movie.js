const connection = require("../utils/connection").con

const createTableOfMovie = () => {
    return new Promise((resolve, reject) => {
        var sql = `CREATE TABLE IF NOT EXISTS movie (movieId INT AUTO_INCREMENT PRIMARY KEY,
    rank varchar(10),
    title varchar(255),
    description varchar (1000),
    runtime varchar(20),
    genre varchar(50),
    rating varchar(10),
    metascore varchar(30),
    votes varchar(30),
    grossEarningInMil varchar(20),
    actor varchar(255),
    year varchar(5),
    directorid int not null,
    FOREIGN KEY directorsId(directorid)
    REFERENCES director(directorid)
    ON UPDATE CASCADE
    ON DELETE CASCADE
)ENGINE=InnoDB;`;
        connection.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result)
            }
        })
    })
}


const getDirectorData = () => {
    return new Promise((resolve, reject) => {
        let sql = 'select * from director';
        connection.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result)
            }
        })
    })
}



const getAllMovie = () => {
    return new Promise((resolve, reject) => {
        let sql = 'select * from movie';
        connection.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result)
            }
        })
    })
}

const getMovieWithId = (id) => {
    return new Promise((resolve, reject) => {
        let sql = `select * from movie where movieId="${id}"`;
        connection.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        })
    })
}
// const addAMovie = async (rank, title, description, runtime, genre, rating, metascore, votes, grossEarningInMil, actor, year, name) => {
//     var directorDetails;
//     await getDirectorData().then(result => {
//         directorDetails = result;
//     })
//     var directorWithId = {}
//     directorDetails.forEach(singleData => {
//         directorWithId[singleData["name"]] = singleData["directorId"];
//     })
//     var directorid = directorWithId[name];
//     return new Promise((resolve, reject) => {
//         let sql = `INSERT INTO movie (rank,title,description,runtime,genre,rating,metascore,votes,grossEarningInMil,actor,year,directorid)
//          VALUES ("${rank}","${title}" ,"${description}","${runtime}","${genre}","${rating}","${metascore}","${votes}","${grossEarningInMil}","${actor}", "${year}","${directorid}");`
//         connection.query(sql, (err, result) => {
//             if (err) {
//                 reject(err);
//             } else {
//                 resolve(result);
//             }
//         })
//     })
// }



const addAMovie = async (movieDetail) => {
    var directorDetails;
    await getDirectorData().then(result => {
        directorDetails = result;
    })
    var directorWithId = {}
    directorDetails.forEach(singleData => {
        directorWithId[singleData.name] = singleData["directorId"];
    })
    var directorid = directorWithId[movieDetail.director];
    return new Promise((resolve, reject) => {
        let sql = `INSERT INTO movie (rank,title,description,runtime,genre,rating,metascore,votes,grossEarningInMil,actor,year,directorid)
         VALUES ("${movieDetail.rank}","${movieDetail.title}" ,"${movieDetail.description}","${movieDetail.runtime}","${movieDetail.genre}","${movieDetail.rating}","${movieDetail.metascore}","${movieDetail.votes}","${movieDetail.grossEarningInMil}","${movieDetail.actor}", "${movieDetail.year}","${directorid}");`
        connection.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        })
    })
}






const updateMovieName = (movieId, singleMovieDetail) => {
    return new Promise((resolve, reject) => {
        let sql = `UPDATE movie SET ${Object.keys(singleMovieDetail)} = "${Object.values(singleMovieDetail)}" where movieId="${movieId}"`;
        connection.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        })
    })
}

const deleMovieWithId = (id) => {
    return new Promise((resolve, reject) => {
        let sql = `delete from  movie where movieId="${id}"`;
        connection.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        })
    })
}

module.exports = {
    createTableOfMovie,
    getDirectorData,
    getAllMovie,
    getMovieWithId,
    addAMovie,
    updateMovieName,
    deleMovieWithId
}