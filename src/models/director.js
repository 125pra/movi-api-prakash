const connection = require("../utils/connection").con

const createTableOfDirector = () => {
  return new Promise((resolve, reject) => {
    var sql = `CREATE TABLE IF NOT EXISTS director (directorId INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255))ENGINE=InnoDB;`;
    connection.query(sql, (err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(result)
      }
    })
  })
}

const getDirectorData = () => {
  return new Promise((resolve, reject) => {
    let sql = 'select * from director';
    connection.query(sql, (err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(result)
      }
    })
  })
}

const getDirectorWithId = (id) => {
  return new Promise((resolve, reject) => {
    let sql = `select * from director where directorId="${id}"`;
    connection.query(sql, (err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(result);
      }
    })
  })

}

const addADirector = (directorName) => {
  return new Promise((resolve, reject) => {
    let sql = `INSERT INTO director (name) VALUES ("${directorName}")`;
    connection.query(sql, (err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(result);
      }
    })
  })

}


const updatDirectorName = (id, directorName) => {
  return new Promise((resolve, reject) => {
    let sql = `UPDATE director SET name = "${directorName}" where directorId="${id}"`;
    connection.query(sql, (err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(result);
      }
    })
  })


}

const deleteDirectorById = (id) => {
  return new Promise((resolve, reject) => {
    let sql = `delete from  director where directorId="${id}"`;
    connection.query(sql, (err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(result);
      }
    })
  })
}

module.exports = {
  createTableOfDirector,
  getDirectorData,
  getDirectorWithId,
  addADirector,
  updatDirectorName,
  deleteDirectorById
};
