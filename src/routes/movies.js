const express = require('express');
const joi = require("joi");
const router = express.Router();
const movie = require("../models/movie");
const isValid = require("../models/validate");
const middlewares = require("../middlewares/middlewares")


router.get('/', (req, res,next) => {
    movie.getAllMovie().then(data => res.json(data)).catch(err => next(err));
})


router.get('/:id', (req, res,next) => {
    const validationResult = joi.validate(req.params, isValid.validateId);
    if (!validationResult.error) {
        movie.getMovieWithId(req.params.id).then((data) => {
            if (data.length === 0) res.status(404).send(`Director with ID ${req.params.id} doesnt exist`);
            else res.json(data);
        }).catch(err => next(err));
    } else res.status(400).send(validationResult.error.details[0].message);
});


router.put('/:id', (req, res,next) => {
    const validationResult = joi.validate(req.params, isValid.validateId);
    if (!validationResult.error) {
        movie.getMovieWithId(req.params.id).then((data) => {
            if (data.length === 0) {
                res.status(404).send('invalid id');
            }
        });
        movie.updateMovieName(req.params.id, req.body).then(data => res.json(data)).catch(err => next(err));
    } else res.status(400).send(validationResult.error.details[0].message);
});


router.post('/', (req, res,next) => {
    const validationResult = joi.validate(req.body, isValid.validateMovie);
    if (!validationResult.error) {
        movie.addAMovie(req.body).then(data => res.json(data)).catch(err => next(err));
    } else res.status(400).send(validationResult.error.details[0].message);
});


router.delete('/:id', (req, res,next) => {
    const validationResult = joi.validate(req.params, isValid.validateId);
    if (!validationResult.error) {
        movie.getMovieWithId(req.params.id).then((data) => {
            if (data.length === 0) {
                res.status(404).send('invalid id');
            }else movie.deleMovieWithId(req.params.id).then(data => res.json(data)).catch(err => next(err));
        }).catch(err => res.json(err));
    }else res.status(400).send(validationResult.error.details[0].message);
});


router.use(middlewares.errorHandler);

module.exports = router;




