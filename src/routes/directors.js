const express = require('express');
const router = express.Router();
const joi = require("joi");
const director = require("../models/director");
const isValid = require("../models/validate");
const middlewares = require("../middlewares/middlewares")

router.get('/', (req, res, next) => {
  director.getDirectorData().then(data => res.json(data)).catch(err => {
    console.log("not a good query");
    next(err)
  });
})


router.get('/:id', (req, res, next) => {
  const validationResult = joi.validate(req.params, isValid.validateId);
  if (!validationResult.error) {
    director.getDirectorWithId(req.params.id).then((data) => {
      if (data.length === 0) res.status(404).send(`Director with Id ${req.params.id} doesnt exist`);
      else res.json(data);
    }).catch(err => next(err));
  } else res.status(400).send(validationResult.error.details[0].message);
})


router.post('/', (req, res,next) => {
  const validationResult = joi.validate(req.body, isValid.validateDirector);
  if (!validationResult.error) {
    director.addADirector(req.body.name).then(data => res.json(data)).catch(err =>next(err) );
  } else res.status(400).send(validationResult.error.details[0].message);
});



router.put('/:id', (req, res,next) => {
  const validationResult = joi.validate(req.params, isValid.validateId);
  if (!validationResult.error) {
    director.getDirectorWithId(req.params.id).then((data) => {
      if (data.length === 0) {
        res.status(404).send('invalid id');
      }
    });

    const validationResult2 = joi.validate(req.body, isValid.validateDirector);
    if (!validationResult2.error) {
      director.updatDirectorName(req.params.id, req.body.directorName).then(data => res.json(data)).catch(err =>next(err));
    } else res.status(400).send(validationResult.error.details[0].message);

  } else res.status(400).send(validationResult.error.details[0].message);

});




router.delete('/:id', (req, res,next) => {
  const validationResult = joi.validate(req.params, isValid.validateId);
  if (!validationResult.error) {
    director.getDirectorWithId(req.params.id).then((data) => {
      if (data.length === 0) {
        res.status(404).send('invalid id');
      } else director.deleteDirectorById(req.params.id)
    }).catch(err => next(err));
  } else res.status(400).send(validationResult.error.details[0].message);
});



router.use(middlewares.errorHandler);

module.exports = router;