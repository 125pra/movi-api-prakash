const express = require('express');
const app = express();
const directorsRoute = require("./src/routes/directors");
const moviesRoute = require("./src/routes/movies");

app.use(express.json());

app.use('/api/directors', directorsRoute);
app.use('/api/movies', moviesRoute);

 


const PORT = process.env.PORT || 5500;
app.listen(PORT, () => console.log(`server started on port ${PORT}`));

module.exports=app;